#!/bin/sh

for file in $(find . -name '*.png'); do
    echo $file
    convert $file +dither -colors 4 $file
done
