#!/bin/sh

set -e

image="openwrt-19.07.5-ipq40xx-generic-avm_fritzbox-4040-squashfs-eva.bin"
dev="eno1"

ip link set down "$dev"
ip addr flush dev "$dev"
ip link set up "$dev"
ip addr add 192.168.178.2/24 dev "$dev"

while ! ping -c 1 -w 1 192.168.178.1 > /dev/null 2>&1; do
	ip link set up "$dev"
	echo "wait 1 second"
	sleep 1
done

sleep 1

ftp -n -v -q 150 -p "192.168.178.1" << EOF
quote USER adam2
quote PASS adam2
binary
quote MEDIA FLSH
put $image mtd1
EOF
