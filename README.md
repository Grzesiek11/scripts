# scripts

This repo contains various shell-, bash- and Python scripts.

Most of them are mine, these that aren't are listed in the [Attributions](#Attributions) section of this file.

## Licensing

This repo is licensed under [ISC](LICENSE).

Some scripts (see the headers) and the *sandbox* section are licensed under [CC-0](LICENSE.CC-0) (and are public domain).

There are also scripts not made by me, licensed under various licenses (see [Attributions](#Attributions)).

You don't need to credit me if you are using parts of my scripts.

## Sections

- **[toolbox](toolbox/)** - Scripts that I actually use, or just want to have at hand. I keep them in my PATH.

- **[random](random/)** - Useful, but not too often. I keep them separated from the *toolbox*.

- **[useless](useless/)** - As the name suggests - not useful at all.

- **[archive](archive/)** - Not useful right now, but I want to keep them.

- **[sandbox](sandbox/)** - Ideas, tests and unfinished prototypes.

## Attributions

- **[toolbox/pfetch](toolbox/pfetch)** - [pfetch](https://github.com/dylanaraps/pfetch) by **dylanaraps**, licensed under [MIT](https://github.com/dylanaraps/pfetch/blob/master/LICENSE.md).

- **[toolbox/pipes](toolbox/pipes)** - [pipes.sh](https://github.com/pipeseroni/pipes.sh) by pipeseroni, licensed under [MIT](https://github.com/pipeseroni/pipes.sh/blob/master/LICENSE).

- **[toolbox/pipesx](toolbox/pipesx)** - [pipesx.sh](https://github.com/pipeseroni/pipesx.sh) by **pipeseroni**, licensed under [MIT](https://github.com/pipeseroni/pipesx.sh/blob/master/LICENSE).

- **[useless/beeps/](useless/beeps/){astronomia,basix,gudmorning,nana,pandas,sandstorm}** - various pcspkr covers by **[TadeLn](https://tadeln.stary.pc.pl)** (more details in comments inside these files). Check out his music!
